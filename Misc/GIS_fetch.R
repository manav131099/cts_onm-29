require('RCurl')
require('utils')
require('mailR')
require('R.utils')
url = "ftp://CLEANTECH:27C0mBA@ftp.geomodel.eu/CLIMDATA/"

	filenames = try(evalWithTimeout(getURLContent(url,ftp.use.epsv = FALSE,dirlistonly = TRUE),
	 timeout = 600, onTimeout = "error"),silent=T)
	if(class(filenames)=='try-error')
	{
		print('Error fetching file from FTP... trying again in 60s')
		exit(0)
	}
  newfilenames = try(unlist(strsplit(filenames,"\n")),silent=T)
	if(class(filenames)=='try-error')
	{
		print('Error fetching file from FTP... trying again in 60s')
		exit(0)
	}
	newfilenamesorig = newfilenames
	#newfile2 = newfilenamesorig[grepl("_116_",newfilenamesorig)]
  #newfile2 = newfile2[grepl("Thailand",newfile2)]
	#print(newfile2)
	#stop(0)
	if(0)
	{
filelistfetch = c(
"SolarGIS_hourly_66_Chachoengsao_Thailand_20180101_20190618.csv",
"SolarGIS_hourly_67_Chachoengsao_Thailand_20180101_20190618.csv",
"SolarGIS_hourly_68_Chang_Wat_Rayong_Thailand_20180101_20190618.csv",
"SolarGIS_hourly_69_Chang_Wat_Rayong_Thailand_20180101_20190618.csv",
"SolarGIS_hourly_70_Samut_Sakhon_Thailand_20180101_20181231.csv",
"SolarGIS_hourly_71_Samut_Sakhon_Thailand_20180101_20181231.csv",
"SolarGIS_hourly_72_Samut_Sakhon_Thailand_20180101_20181231.csv",
"SolarGIS_hourly_73_Samut_Sakhon_Thailand_20180101_20181231.csv",
"SolarGIS_hourly_74_Penang_Malaysia_0az_20190601.csv",
"SolarGIS_hourly_75_Penang_Malaysia_90az_20190601.csv",
"SolarGIS_hourly_76_Penang_Malaysia_180az_20190601.csv",
"SolarGIS_hourly_77_Penang_Malaysia_270az_20190601.csv",
"SolarGIS_hourly_78_Rayong_Thailand_0az_20190601.csv",
"SolarGIS_hourly_79_Rayong_Thailand_180az_20190601.csv",
"SolarGIS_hourly_80_Cebu_Phillipines_0az_20190601.csv",
"SolarGIS_hourly_81_Cebu_Phillipines_90az_20190601.csv",
"SolarGIS_hourly_82_Cebu_Phillipines_180az_20190601.csv",
"SolarGIS_hourly_83_Cebu_Phillipines_270az_20190601.csv",
"SolarGIS_hourly_84_Manila_Phillipines_0az_20190601.csv",
"SolarGIS_hourly_85_Manila_Phillipines_90az_20190601.csv",
"SolarGIS_hourly_86_Manila_Phillipines_180az_20190601.csv",
"SolarGIS_hourly_87_Manila_Phillipines_270az_20190601.csv",
"SolarGIS_hourly_88_NR_31_road_Cambodia_0az_20190601.csv",
"SolarGIS_hourly_89_NR_31_road_Cambodia_90az_20190601.csv",
"SolarGIS_hourly_90_NR_31_road_Cambodia_180az_20190601.csv",
"SolarGIS_hourly_91_NR_31_road_Cambodia_270az_20190601.csv",
"SolarGIS_hourly_92_Samut_Sakhon_Thailand_0az_20190601.csv",
"SolarGIS_hourly_93_Samut_Sakhon_Thailand_90az_20190601.csv",
"SolarGIS_hourly_94_Samut_Sakhon_Thailand_180az_20190601.csv",
"SolarGIS_hourly_95_Samut_Sakhon_Thailand_270az_20190601.csv"
)

filenamesdownload = c(
"ZSolarGIS_TS_hourly_CLEANTECH_Chachoengsao_Thailand_180a_20180101_20190618.csv",
"ZSolarGIS_TS_hourly_CLEANTECH_Chachoengsao_Thailand_0a_20180101_20190618.csv",
"ZSolarGIS_TS_hourly_CLEANTECH_Chang-Wat-Rayong_Thailand_180a_20190101_20190131.csv",
"ZSolarGIS_TS_hourly_CLEANTECH_Chang-Wat-Rayong_Thailand_0a_20190101_20190131.csv",
"ZSolarGIS_TS_hourly_CLEANTECH_SamutSakhon_Thailand_270az_20190101_20181231.csv",
"ZSolarGIS_TS_hourly_CLEANTECH_SamutSakhon_Thailand_180az_20190101_20181231.csv",
"ZSolarGIS_TS_hourly_CLEANTECH_SamutSakhon_Thailand_90az_20190101_20181231.csv",
"ZSolarGIS_TS_hourly_CLEANTECH_SamutSakhon_Thailand_0az_20190101_20181231.csv",
"SolarGIS_TS_hourly_CLEANTECH_Penang_Malaysia_0az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_Penang_Malaysia_90az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_Penang_Malaysia_180az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_Penang_Malaysia_270az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_Rayong_Thailand_0az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_Rayong_Thailand_180az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_Cebu_Phillipines_0az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_Cebu_Phillipines_90az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_Cebu_Phillipines_180az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_Cebu_Phillipines_270az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_Manila_Phillipines_0az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_Manila_Phillipines_90az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_Manila_Phillipines_180az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_Manila_Phillipines_270az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_CMIC_Cambodia_0az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_CMIC_Cambodia_90az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_CMIC_Cambodia_180az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_CMIC_Cambodia_270az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_SamutSakhon_Thailand_0az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_SamutSakhon_Thailand_90az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_SamutSakhon_Thailand_180az_20190601_20190601.csv",
"SolarGIS_TS_hourly_CLEANTECH_SamutSakhon_Thailand_270az_20190601_20190601.csv"
)
}

filelistfetch = c("SolarGIS_hourly_96_My_Phuoc_2_Industrial_park_Vietnam_20190101_20190131.csv",
"SolarGIS_hourly_100_Tay_Ninh_Vietnam_10t_270a_20190728.csv",
"SolarGIS_hourly_97_Tay_Ninh_Vietnam_10t_0a_20190728.csv",
"SolarGIS_hourly_98_Tay_Ninh_Vietnam_10t_90a_20190728.csv",
"SolarGIS_hourly_99_Tay_Ninh_Vietnam_10t_180a_20190728.csv"
)

filenamesdownload = c("ZSolarGIS_TS_hourly_CLEANTECH_My-Phuoc-2-Industrial-park_Vietnam_20190101_20190131.csv",
"SolarGIS_TS_hourly_CLEANTECH_Tay-Ninh_Vietnam_10t_270a_20190728_20190728.csv",
"ZSolarGIS_TS_hourly_CLEANTECH_Tay-Ninh_Vietnam_10t_0a_20190728_20190728.csv",
"SolarGIS_TS_hourly_CLEANTECH_Tay-Ninh_Vietnam_10t_90a_20190728.csv",
"SolarGIS_TS_hourly_CLEANTECH_Tay-Ninh_Vietnam_10t_180a_20190728.csv"
)
 filelistfetch = c("SolarGIS_hourly_115_Tirunelveli_India_20200117.csv",
 "SolarGIS_hourly_116_Kuranganwali_India_20200117.csv"
)
 filenamesdownload = c("SolarGIS_TS_hourly_CLEANTECH_Tirunelveli_India_20200117_20200117.csv",
  "SolarGIS_TS_hourly_CLEANTECH_Kuranganwali_India_20200117_20200117.csv")
 

pathdown = "/tmp"

for(x in 1 : length(filelistfetch))
{
urlnew = paste(url,filelistfetch[x],sep="")
destpath = paste(pathdown,filenamesdownload[x],sep="/")
download.file(urlnew,destfile = destpath)
}
