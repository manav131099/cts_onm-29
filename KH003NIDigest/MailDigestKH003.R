rm(list = ls())

errHandle = file('/home/admin/Logs/LogsKH003NI.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

source('/home/admin/CODE/KH003NIDigest/HistoricalAnalysis2G3GKH003.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
require('mailR')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/KH003NIDigest/aggregateInfo.R')
print('History done')
InstCap = 25.60
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
  min = as.numeric(hr[2])
  hr = as.numeric(hr[1])
  return((hr * 60 + min + 1))
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf3 = function(x)
{
  return(format(round(x,3),nsmall=3))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
initDigest = function(df)
{
#	rat = round(as.numeric(df[,8])/(InstCap * as.numeric(extra[3])),1)
#	rat2 = round((abs(as.numeric(extra[2]) - as.numeric(extra[4])))/(InstCap * as.numeric(extra[3])),1)
#	rat3 =  round((abs(as.numeric(extra[2]) - as.numeric(extra[4])))/(InstCap*100),2)
  PR = format(round((as.numeric(df[,5])/(as.numeric(df[,4])*InstCap)),1),nsmall=1)
	YLD = round((as.numeric(df[,5])/InstCap/100),2)
  PR2 = format(round((as.numeric(df[,6])/(as.numeric(df[,4])*InstCap)),1),nsmall=1)
	YLD2 = round((as.numeric(df[,6])/InstCap/100),2)
  PRSUBS = format(round((as.numeric(df[,10])/(as.numeric(df[,4])*InstCap)),1),nsmall=1)
  PRSUBS2 = format(round((as.numeric(df[,11])/(as.numeric(df[,4])*InstCap)),1),nsmall=1)
	YLDSUBS = round((as.numeric(df[,10])/InstCap/100),2)
	YLDSUBS2 = round((as.numeric(df[,6])/InstCap/100),2)
	body = "\n\n__________________________________________________________\n"
  body = paste(body,as.character(df[,1]))
  body = paste(body,"\n__________________________________________________________\n\n")
  body = paste(body,"Data availability: ",as.character(df[,2]),"%",sep="")
	body = paste(body,"\n\nDowntime [%]:",as.character(df[,3]))
	body = paste(body,"\n\nDaily Irradiation Silicon Sensor [kWh/m2]: ",as.character(df[,4]))
  body = paste(body,"\n\nEac Inverter Room - Method 1 [kWh]: ",as.character(df[,5]),sep="")
  body = paste(body,"\n\nYield Inverter Room - Method 1 [kWh/kWp]: ",as.character(YLD),sep="")
  body = paste(body,"\n\nPR Inverter Room - Method 1 [%]: ",as.character(PR),sep="")
	body = paste(body,"\n\nEac Inverter Room -  Method 2 [kWh]: ",as.character(df[,6]),sep="")
  body = paste(body,"\n\nYield Inverter Room - Method 2 [kWh/kWp]: ",as.character(YLD2),sep="")
  body = paste(body,"\n\nPR Inverter Room - Method 2 [%]: ",as.character(PR2),sep="")
  body = paste(body,"\n\nRatio Inverter Room 1/2 Eac: ",as.character(df[,7]),sep="")
  body = paste(body,"\n\nEac Substation - Method 1 [kWh]: ",as.character(df[,10]),sep="")
  body = paste(body,"\n\nYield Substation - Method 1 [kWh/kWp]: ",as.character(YLDSUBS),sep="")
  body = paste(body,"\n\nPR Substation - Method 1 [%]: ",as.character(PRSUBS),sep="")
	body = paste(body,"\n\nEac Substation - Method 2 [kWh]: ",as.character(df[,11]),sep="")
  body = paste(body,"\n\nYield Substation - Method 2 [kWh/kWp]: ",as.character(YLDSUBS2),sep="")
  body = paste(body,"\n\nPR Substation - Method 2 [%]: ",as.character(PRSUBS2),sep="")
	body = paste(body,"\n\nRatio Substation - Method1/2 Eac: ",as.character(df[,12]),sep="")
	body = paste(body,"\n\nCable Losses [%]: ",as.character(df[,15]),sep="")
	body = paste(body,"\n\nLast Recorded Timestamp Inverter Room: ",as.character(df[,8]),sep="")
  body = paste(body,"\n\nLast Recorded Reading Inverter Room [kWh]: ",as.character(df[,9]),sep="")
	body = paste(body,"\n\nLast Recorded Timestamp Substation: ",as.character(df[,13]),sep="")
  body = paste(body,"\n\nLast Recorded Reading Substation [kWh]: ",as.character(df[,14]),sep="")
  body = paste(body,"\n\n___",sep="")
	body = paste(body,"\n\nMean Tamb [C]: ",as.character(df[,26]),sep="")
  body = paste(body,"\n\nMax Tamb [C]: ",as.character(df[,34]),sep="")
  body = paste(body,"\n\nTime of max Tamb occurance: ",as.character(df[,37]),sep="")
  body = paste(body,"\n\nMean Tamb solar hours [C]: ",as.character(df[,27]),sep="")
  body = paste(body,"\n\n___",sep="")
  body = paste(body,"\n\nMean Hamb [%]: ",as.character(df[,18]),sep="")
  body = paste(body,"\n\nMean Hamb solar hours [%]: ",as.character(df[,19]),sep="")
  body = paste(body,"\n\nMax Hamb solar hours [%]: ",as.character(df[,41]),sep="")
  body = paste(body,"\n\nTime of max Hamb occurance: ",as.character(df[,43]),sep="")
  body = paste(body,"\n\nMin Hamb solar hours [%]: ",as.character(df[,42]),sep="")
  body = paste(body,"\n\nTime of min Hamb occurance: ",as.character(df[,44]),sep="")
  body = paste(body,"\n\n___",sep="")
  body = paste(body,"\n\nMean Tmod [C]: ",as.character(df[,24]),sep="")
  body = paste(body,"\n\nMax Tmod [C]: ",as.character(df[,32]),sep="")
  body = paste(body,"\n\nTime of max Tmod occurance: ",as.character(df[,38]),sep="")
  body = paste(body,"\n\nMean Tmod solar hours [C]: ",as.character(df[,25]),sep="")
  body = paste(body,"\n\n___",sep="")
  body = paste(body,"\n\nMean Troom [C]: ",as.character(df[,16]),sep="")
  body = paste(body,"\n\nMax Troom [C]: ",as.character(df[,30]),sep="")
  body = paste(body,"\n\nTime of max Troom occurance: ",as.character(df[,39]),sep="")
  body = paste(body,"\n\nMean Troom solar hours [C]: ",as.character(df[,17]),sep="")
  body = paste(body,"\n\n___",sep="")
  body = paste(body,"\n\nMean Hroom [%]: ",as.character(df[,20]),sep="")
  body = paste(body,"\n\nMean Hroom solar hours [%]: ",as.character(df[,21]),sep="")
  body = paste(body,"\n\n___",sep="")
  body = paste(body,"\n\nMean Tcab [C]: ",as.character(df[,28]),sep="")
  body = paste(body,"\n\nMax Tcab [C]: ",as.character(df[,36]),sep="")
  body = paste(body,"\n\nTime of max Tcab occurance: ",as.character(df[,40]),sep="")
  body = paste(body,"\n\nMean Tcab solar hours [C]: ",as.character(df[,29]),sep="")
  body = paste(body,"\n\n___",sep="")
  body = paste(body,"\n\nMean Hcab [%]: ",as.character(df[,22]),sep="")
  body = paste(body,"\n\nMean Hcab solar hours [%]: ",as.character(df[,23]),sep="")
  return(body)
}
printtsfaults = function(TS,body)
{
	if(length(TS) > 1)
	{
		body = paste(body,"\n\n__________________________________________________________\n")
		body = paste(body,paste("\nTimestamps","where EacM2 < 5 between 8am - 5pm\n"))
		for(lm in  1 : length(TS))
		{
			body = paste(body,TS[lm],sep="\n")
		}
	}
	return(body)
}
sendMail = function(filetosendpath)
{
  body = ""
	body = paste(body,"Site Name: Coca-Cola Cambodia",sep="")
	body = paste(body,"\n\nLocation: Phnom Penh, Cambodia")
	body = paste(body,"\n\nO&M Code: KH-003")
	body = paste(body,"\n\nSystem Size: 2560 kWp")
	body = paste(body,"\n\nNumber of Energy Meters: 2")
	body = paste(body,"\n\nModule Brand / Model / Nos: Canadian Solar / CS6X-320P / 8200")
	body = paste(body,"\n\nInverter Brand / Model / Nos: SMA / STP60-10 / 36")
	body = paste(body,"\n\nSite COD:",as.character(DOB))
	body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(DAYSALIVE))))
	body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(DAYSALIVE))/365,2)))

  filenams = c()
  for(l in 1 : length(filetosendpath))
  {
    temp = unlist(strsplit(filetosendpath[l],"/"))
    filenams[l] = temp[length(temp)]
		if(l == 1)
			currday = temp[length(temp)]
  }
	df1 = read.table(filetosendpath[1],header = T,sep="\t",stringsAsFactors=F)
	print('Filenames Processed')
  body = paste(body,initDigest(df1),sep="")
	DATETOPRINTSUBJ = as.character(df1[,1])
  body = printtsfaults(TIMESTAMPSALARM,body)
	if(length(filetosendpath) > 1)
	{
		df1 = read.table(filetosendpath[2],header = T,sep="\t")
		body = paste(body,initDigest(df1))
	}
  graph_command1 = paste("Rscript /home/admin/CODE/KH003NIDigest/PR_Graph_Azure.R", "KH-003", 64.3, 0.008, "2016-10-06", DATETOPRINTSUBJ, sep=" ")
  system(graph_command1)
  graph_path1=paste('/home/admin/Graphs/Graph_Output/KH-003/[KH-003] Graph ',DATETOPRINTSUBJ,' - PR Evolution.pdf',sep="")
  graph_extract_path1=paste('/home/admin/Graphs/Graph_Extract/KH-003/[KH-003] Graph ',DATETOPRINTSUBJ,' - PR Evolution.txt',sep="")
  filetosendpath = c(graph_path1,graph_extract_path1,filetosendpath)
  body = paste(body,"\n\n__________________________________________________________\n\n")
  body = paste(body,"Station History")
  body = paste(body,"\n\n__________________________________________________________\n\n")
  body = paste(body,"Station DOB:",as.character(DOB))
  body = paste(body,"\n# Days alive:",as.character(DAYSALIVE))
  yrsalive = format(round((DAYSALIVE/365),2),nsmall=2)
  body = paste(body,"\n# Years alive:",yrsalive)
  body = gsub("\n ","\n",body)
	send.mail(from = sender,
            to = recipients,
            subject = paste("Station [KH-003S] Digest",DATETOPRINTSUBJ),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filetosendpath,
            debug = F)
recordTimeMaster("KH-003S","Mail",DATETOPRINTSUBJ)

}
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = getRecipients("KH-003S","m")
#recipients = c('shravan1994@gmail.com')

pwd = 'CTS&*(789'
todisp = 1
path = "/home/admin/Dropbox/Cleantechsolar/1min/[715]"
pathwrite = "/home/admin/Dropbox/Second Gen/[KH-003S]"
pathwrite3G = pathwrite
checkdir(pathwrite)
date = format(Sys.time(),tz = "Singapore")
years = dir(path)
prevpathyear = paste(path,years[length(years)],sep="/")
months = dir(prevpathyear)
prevpathmonth = paste(prevpathyear,months[length(months)],sep="/")
currday = prevday = dir(prevpathmonth)[length(dir(prevpathmonth))]
prevwriteyears = paste(pathwrite,years[length(years)],sep="/")
prevwritemonths = paste(prevwriteyears,months[length(months)],sep="/")
prevwriteyears3G = paste(pathwrite3G,years[length(years)],sep="/")

filenam = paste(substr(as.character(years[length(years)]),3,4),substr(as.character(months[length(months)]),6,7),sep="")

prevwritemonths3G = paste(prevwritemonths,"/[KH-003S] ",filenam,".txt",sep="")

stnnickName = "715"
stnnickName2 = "KH-003S"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
if(!is.null(lastdatemail))
{
	yr = substr(lastdatemail,1,4)
	monyr = substr(lastdatemail,1,7)
	prevpathyear = paste(path,yr,sep="/")
	prevpathmonth = paste(prevpathyear,monyr,sep="/")
	currday = prevday = paste("[",stnnickName,"] ",lastdatemail,".txt",sep="")
	print(paste("prevday",prevday,"currday", currday))
	prevwriteyears = paste(pathwrite,yr,sep="/")
	prevwritemonths = paste(prevwriteyears,monyr,sep="/")
	filenam = paste(substr(as.character(years[length(years)]),3,4),substr(as.character(months[length(months)]),6,7),sep="")
	prevwritemonths3G = paste(prevwritemonths,"/[",stnnickName2,"] ",filenam,".txt",sep="")
}

check = 0
pwd = 'CTS&*(789'

wt =1
while(1)
{
recipients = getRecipients("KH-003S","m")
recordTimeMaster("KH-003S","Bot")
years = dir(path)
writeyears = paste(pathwrite,years[length(years)],sep="/")
writeyears3G = paste(pathwrite,years[length(years)],sep="/")
checkdir(writeyears)
checkdir(writeyears3G)
pathyear = paste(path,years[length(years)],sep="/")
months = dir(pathyear)
writemonths = paste(writeyears,months[length(months)],sep="/")
filenam = paste(substr(as.character(years[length(years)]),3,4),substr(as.character(months[length(months)]),6,7),sep="")
writemonths3G = paste(writemonths,"/[KH-003S] ",filenam,".txt",sep="")
checkdir(writemonths)
pathmonth = paste(pathyear,months[length(months)],sep="/")
tm = timetomins(substr(format(Sys.time(),tz = "Singapore"),12,16))
currfiles = dir(pathmonth)
currfiles = currfiles[grepl("[715]",currfiles)]
currday = currfiles[length(currfiles)]
	print(paste("prevday",prevday,"currday", currday))
if(length(currday) == 0 || prevday == currday)
{
   if(wt == 1)
{
 print("")
 print("")
 print("__________________________________________________________")
 print(substr(currday,7,16))
 print("__________________________________________________________")
 print("")
 print("")
 print('Waiting')
wt = 0
}
{
  if(tm > 1319 || tm < 30)
  {
      Sys.sleep(120)
  }        
  else
  {
    Sys.sleep(3600)
  }
}
next
}
print('New Data found.. sleeping to ensure sync complete')
Sys.sleep(20)
print('Sleep Done')
				currdayw = gsub("715","KH-003S",currday)
lastdata = read.table(paste(pathmonth,currday,sep="/"),header = T,sep="\t")
print(colnames(lastdata))
dat = as.numeric(lastdata[complete.cases(lastdata[,3]),3])
colno = NA

datawrite = secondGenData(paste(pathmonth,currday,sep="/"),paste(writemonths,currdayw,sep="/"))
datasum = thirdGenData(paste(writemonths,currdayw,sep="/"),writemonths3G)
DAYSALIVE = DAYSALIVE + 1
print('Digest Written');
filetosendpath = c(paste(writemonths,currdayw,sep="/"))
filename = c(currdayw)
filedesc = c("Daily Digest")
				prevdayw = gsub("715","KH-003S",prevday)
dataprev = read.table(paste(prevwritemonths,prevdayw,sep="/"),header = T,sep = "\t")
dataprev2 = read.table(paste(prevpathmonth,prevday,sep="/"),header = T,sep = "\t")
colno2 = round(sum(dataprev2[complete.cases(dataprev2[,3]),3])/60000,2)

datawrite = secondGenData(paste(prevpathmonth,prevday,sep="/"),paste(prevwritemonths,prevdayw,sep="/"))
print('Summary read')
if(as.numeric(datawrite[,2]) != as.numeric(dataprev[,2]))
{
print('Difference in old file noted')
tots = thirdGenData(paste(prevwritemonths,prevdayw,sep="/"),prevwritemonths3G)
lastdata[8] = as.character(dataprev2[nrow(dataprev2),1])
lastdata[9] = as.character(dataprev2[nrow(dataprev2),39])
lastdata[4] = colno2
lastdata[13] = as.character(dataprev2[nrow(dataprev2),1])
lastdata[14] = as.character(dataprev2[1,86])
  filetosendpath[2] = paste(prevwritemonths,prevdayw,sep="/")
  filename[2] = prevdayw
  filedesc[2] = c("Updated Digest")
print('Updated old files.. both digest and summary file')
}
sendMail(filetosendpath)
print('Mail Sent');
prevday = currday
prevpathyear = pathyear
prevpathmonth = pathmonth
prevwriteyears = writeyears
prevwritemonths = writemonths
prevwriteyears3G = writeyears3G
prevwritemonths3G = writemonths3G
wt = 1
gc()
}
sink()
