source('/home/admin/CODE/common/math.R')

checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
prepareSumm = function(dataread)
{
	da = nrow(dataread)
	daPerc=da/14.4
  thresh = 5/1000
	gsi1 = tamb=tambst=hamb=hambst=tambmx=tambstmx=hambmx=hambstmx=tambmn=tambstmn=hambmn=hambstmn=tsi1=NA
  
	dataread2 = dataread[complete.cases(dataread[,3]),3]
	
	if(length(dataread2))
		gsi1 = sum(dataread[complete.cases(dataread[,3]),3])/60000
#  gsi2 = sum(dataread[complete.cases(dataread[,4]),4])/60000
#  gsismp = sum(dataread[complete.cases(dataread[,5]),5])/60000
  subdata = dataread[complete.cases(dataread[,3]),]
  subdata = subdata[as.numeric(subdata[,3]) > thresh,]
	
	dataread2 = dataread[complete.cases(dataread[,4]),4]
	if(length(dataread2))
	tamb = mean(dataread[complete.cases(dataread[,4]),4])
	
	dataread2 = subdata[complete.cases(subdata[,4]),4]
	if(length(dataread2))
  tambst = mean(subdata[complete.cases(subdata[,4]),4])
  
	dataread2 = dataread[complete.cases(dataread[,5]),5]
	if(length(dataread2))
  hamb = mean(dataread[complete.cases(dataread[,5]),5])
  
	dataread2 = subdata[complete.cases(subdata[,5]),5]
	if(length(dataread2))
	hambst = mean(subdata[,5])
  
	dataread2 = dataread[complete.cases(dataread[,4]),4]
	if(length(dataread2))
  tambmx = max(dataread[complete.cases(dataread[,4]),4])
  
	dataread2 = subdata[complete.cases(subdata[,4]),4]
	if(length(dataread2))
	tambstmx = max(subdata[,4])
  
	dataread2 = dataread[complete.cases(dataread[,5]),5]
	if(length(dataread2))
  hambmx = max(dataread[complete.cases(dataread[,5]),5])
  
	
	dataread2 = subdata[complete.cases(subdata[,5]),5]
	if(length(dataread2))
	hambstmx = max(subdata[,5])
  
	dataread2 = dataread[complete.cases(dataread[,4]),4]
	if(length(dataread2))
  tambmn = min(dataread[complete.cases(dataread[,4]),4])
  
	dataread2 = subdata[complete.cases(subdata[,4]),4]
	if(length(dataread2))
	tambstmn = min(subdata[,4])
  
	dataread2 = dataread[complete.cases(dataread[,5]),5]
	if(length(dataread2))
  hambmn = min(dataread[complete.cases(dataread[,5]),5])
  
	dataread2 = subdata[complete.cases(subdata[,5]),5]
	if(length(dataread2))
	hambstmn = min(subdata[,5])
  
	dataread2 = dataread[complete.cases(dataread[,6]),6]
	if(length(dataread2))
  tsi1 = mean(dataread[complete.cases(dataread[,6]),6])
#  tsi2 = mean(dataread[complete.cases(dataread[,3]),9])
  
#  gsirat = gsi1 / gsi2
#  smprat = gsismp / gsi2

	Eac11 = Eac12 = Eac13 = Eac21 = Eac22 = Eac23 = PR11 = PR12 = PR13 = PR21 = PR22 = PR23 = NA
	
	dataread2 = dataread[complete.cases(dataread[,20]),20]
	if(length(dataread2))
	Eac11 = sum(dataread[complete.cases(dataread[,20]),20])/-60
	
	dataread2 = dataread[complete.cases(dataread[,67]),67]
	if(length(dataread2))
  Eac12 = sum(dataread[complete.cases(dataread[,67]),67])/-60
	
	dataread2 = dataread[complete.cases(dataread[,114]),114]
	if(length(dataread2))
  Eac13 = sum(dataread[complete.cases(dataread[,114]),114])/-60
	LastR1=LastR2=LastR3=LastT1=LastT2=LastT3=NA
	dataread2 = dataread[complete.cases(dataread[,36]),36]
	cond = ( dataread2 > 0 &  dataread2 < 1000000000)
	dataread2 =  dataread2[cond]
	time = dataread[complete.cases(dataread[,36]),1]
	
	if(length(dataread2))
	{
  Eac21 = as.numeric(dataread2[length(dataread2)])  - as.numeric(dataread2[1])
	LastR1 = as.numeric(dataread2[length(dataread2)])
	LastT1 = as.character(time[length(time)])
	}
	dataread2 = dataread[complete.cases(dataread[,83]),83]
	cond = ( dataread2 > 0 &  dataread2 < 1000000000)
	dataread2 =  dataread2[cond]
	time = dataread[complete.cases(dataread[,83]),1]
	if(length(dataread2))
  {
	  Eac22 = as.numeric(dataread2[length(dataread2)])  - as.numeric(dataread2[1])
	  LastR2 = as.numeric(dataread2[length(dataread2)])
		LastT2 = as.character(time[length(time)])
	}
	dataread2 = dataread[complete.cases(dataread[,130]),130]
	cond = ( dataread2 > 0 &  dataread2 < 1000000000)
	dataread2 =  dataread2[cond]
	time = dataread[complete.cases(dataread[,130]),1]
	if(length(dataread2))
  { 
   Eac23 = as.numeric(dataread2[length(dataread2)])  - as.numeric(dataread2[1])
	 LastR3 = as.numeric(dataread2[length(dataread2)])
	 LastT3 = as.character(time[length(time)])
  }
	PR11 = (Eac11 * 100) / (gsi1*839.52)
	PR12 = (Eac12 * 100) / (gsi1*782.28)
	PR13 = (Eac13 * 100) / (gsi1*165.36)
	PR21 = (Eac21 * 100) / (gsi1*839.52)
	PR22 = (Eac22 * 100) / (gsi1*782.28)
	PR23 = (Eac23 * 100) / (gsi1*165.36)
	YLD11 = 0.01*PR11 * gsi1
	YLD12 = 0.01*PR12 * gsi1
	YLD13 = 0.01*PR13 * gsi1
	YLD21 = 0.01*PR21 * gsi1
	YLD22 = 0.01*PR22 * gsi1
	YLD23 = 0.01*PR23 * gsi1
	FullSiteProd = Eac21+Eac22
	CarParkProd=Eac23
	MainRoofProd = FullSiteProd-CarParkProd
	dateAc = NA
	if(nrow(dataread))
		dateAc = substr(dataread[1,1],1,10)

	Eac1MBD = Eac12 - Eac13
	Eac2MBD = Eac22 - Eac23
	PR1MBD = (Eac1MBD * 100) / (gsi1 * 616.92)
	PR2MBD = (Eac2MBD * 100) / (gsi1 * 616.92)
	YLD1MBD = PR1MBD * 0.01 * gsi1
	YLD2MBD = PR2MBD * 0.01 * gsi1

	ylds = c(YLD21,YLD23,YLD2MBD)
	sddev = sdp(ylds)
	cov = sddev*100/mean(ylds)
 
  timestamps_irradiance_greater_20 = dataread[dataread[, 3]>20, 1]
  
  #Meter_A
  timestamps_freq_greater_40 = dataread[(dataread[,33]>40 & dataread[,33]!= "NaN") ,1]
  timestamps_pow_greater_2 = dataread[abs(dataread[,20])>2,1]
  common = intersect(timestamps_irradiance_greater_20, timestamps_freq_greater_40)
  common2 = intersect(common, timestamps_pow_greater_2)
  GA_1 = round(((length(common)/length(timestamps_irradiance_greater_20))*100), 1)
  PA_1 = round(((length(common2)/length(common))*100), 1)
  
  #Meter_B
  timestamps_freq_greater_40 = dataread[dataread[,80]>40,1]
  timestamps_pow_greater_2 = dataread[abs(dataread[,67])>2,1]
  common = intersect(timestamps_irradiance_greater_20, timestamps_freq_greater_40)
  common2 = intersect(common, timestamps_pow_greater_2)
  GA_2 = round(((length(common)/length(timestamps_irradiance_greater_20))*100), 1)
  PA_2 = round(((length(common2)/length(common))*100), 1)  
  
  #Meter_C
  timestamps_freq_greater_40 = dataread[dataread[,127]>40,1]
  timestamps_pow_greater_2 = dataread[abs(dataread[,114])>2,1]
  common = intersect(timestamps_irradiance_greater_20, timestamps_freq_greater_40)
  common2 = intersect(common, timestamps_pow_greater_2)
  GA_3 = round(((length(common)/length(timestamps_irradiance_greater_20))*100), 1)
  PA_3 = round(((length(common2)/length(common))*100), 1)
  
  INSTCAPM = c(839.52, 782.28, 165.36)
  W_G = round(((GA_1 * INSTCAPM[1] + GA_2 * INSTCAPM[2] + GA_3 * INSTCAPM[3])/sum(INSTCAPM)), 1)
  W_P = round(((PA_1 * INSTCAPM[1] + PA_2 * INSTCAPM[2] + PA_3 * INSTCAPM[3])/sum(INSTCAPM)), 1)

  
  #Calculating the dates where GA is less than 95
  #if ((W_G != "NaN" && W_G<95))
  #  print(dateAc)
   
  datawrite = data.frame(Date = dateAc,PtsRec = rf(da),Gsi00 = rf(gsi1), 
#	Gsi02 = rf(gsi2),Smp = rf(gsismp),
                         Tamb = rf1(tamb), TambSH = rf1(tambst),TambMx = rf1(tambmx), TambMn = rf1(tambmn),
                         TambSHmx = rf1(tambstmx), TambSHmn = rf1(tambstmn), Hamb = rf1(hamb), HambSH = rf1(hambst),
                         HambMx = rf1(hambmx), HambMn = rf1(hambmn), HambSHmx = rf1(hambstmx), HambSHmn = rf1(hambstmn),
                         TMod = rf1(tsi1),
												 Eac1MA = rf(Eac11),
												 Eac1MB = rf(Eac12),
												 Eac1MC = rf(Eac13),
												 Eac2MA = rf(Eac21),
												 Eac2MB = rf(Eac22),
												 Eac2MC = rf(Eac23),
												 PR1MA = rf1(PR11),
												 PR1MB = rf1(PR12),
												 PR1MC = rf1(PR13),
												 PR2MA = rf1(PR21),
												 PR2MB = rf1(PR22),
												 PR2MC = rf1(PR23),
												 FullSiteProd = rf(FullSiteProd),
												 CarParkProd = rf(CarParkProd),
												 MainRoofProd = rf(MainRoofProd),
												 YLD11 = rf(YLD11),
												 YLD12 = rf(YLD12),
												 YLD13 = rf(YLD13),
												 YLD21 = rf(YLD21),
												 YLD22 = rf(YLD22),
												 YLD23 = rf(YLD23),
												 LastR1=LastR1,
												 LastR2=LastR2,
												 LastR3=LastR3,
												 Eac1MBD = rf(Eac1MBD),
												 Eac2MBD = rf(Eac2MBD),
												 YLD1MBD = rf(YLD1MBD),
												 YLD2MBD = rf(YLD2MBD),
												 PR1MBD = rf1(PR1MBD),
												 PR2MBD = rf1(PR2MBD),
												 SDYlds = rf(sddev),
												 CovYlds = rf1(cov),
												 LastT1 = LastT1,
												 LastT2 = LastT2,
												 LastT3 = LastT3,
												 DA=rf1(daPerc),
                         GA_1 = GA_1,
                         GA_2 = GA_2,
                         GA_3 = GA_3,
                         PA_1 = PA_1,
                         PA_2 = PA_2,
                         PA_3 = PA_3,
                         W_G = W_G,
                         W_P = W_P,                                          
                         stringsAsFactors=F
												# Tsi02= rf(tsi2), GsiRat = rf(gsirat), SpmRat = rf(smprat),
												)
  datawrite
}
rewriteSumm = function(datawrite)
{
  
  df = datawrite
  df
}

path = "/home/admin/Dropbox/Cleantechsolar/1min/[716]"
pathwrite = "/home/admin/Dropbox/Second Gen/[SG-003S]"
checkdir(pathwrite)
years = dir(path)
x=y=z=1
for(x in 1 : length(years))
{
  pathyear = paste(path,years[x],sep="/")
  writeyear = paste(pathwrite,years[x],sep="/")
  checkdir(writeyear)
  months = dir(pathyear)
  for(y in  1: length(months))
  {
    pathmonth = paste(pathyear,months[y],sep="/")
    writemonth = paste(writeyear,months[y],sep="/")
    checkdir(writemonth)
    days = dir(pathmonth)
    sumfilename = paste("[SG-003S] ",substr(months[y],3,4),substr(months[y],6,7),".txt",sep="")
    for(z in 1 : length(days))
    {
      dataread = read.table(paste(pathmonth,days[z],sep="/"),sep="\t",header = T)
      datawrite = prepareSumm(dataread)
      datasum = rewriteSumm(datawrite)
			currdayw = gsub("716","SG-003S",days[z])
      write.table(datawrite,file = paste(writemonth,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      {
        if(!file.exists(paste(writemonth,sumfilename,sep="/")) || (x == 1 && y == 1 && z==1))
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        }
        else 
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        }
      }
    }
  }
}
