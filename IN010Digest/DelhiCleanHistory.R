rm(list = ls())
errHandle = file('/home/admin/Logs/LogsDelhiHistory.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

require('mailR')
source('/home/admin/CODE/IN010Digest/FTPDelhidump.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
LTCUTOFF = 0.001
FIREERRATA = 1
THRESHOLDCOUNT = 10
FIRETWILIOCOUNT = 0
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

timetomins = function(x)
{
  hrs = unlist(strsplit(x,":"))
  mins = as.numeric(hrs[2])
  hrs = as.numeric(hrs[1]) * 60
  return((((hrs + mins)/5)+1))
}
checkErrata = function(row,ts)
{
  if(ts < 108 || ts > 192)
	{return()}
	if(FIREERRATA == 0)
	{
	  print(paste('Errata mail already sent','so no more mails'))
		return()
	}
	if((!(is.finite(as.numeric(row[2])))) || (as.numeric(row[2]) < LTCUTOFF))
	{
		FIRETWILIOCOUNT = FIRETWILIOCOUNT + 1
	}
	if(FIRETWILIOCOUNT > THRESHOLDCOUNT)
	{
		FIREERRATA <<- 0
		FIRETWILIOCOUNT <<-0
		subj = paste('IN-010X','down')
    body = 'More than 10 readings faulty\nLast recorded timestamp and reading\n'
		body = paste(body,'Timestamp:',as.character(row[1]))
		body = paste(body,'Real Power Tot kW reading:',as.character(row[2]))
		counter = 0
		while(1)
		{
		sendMail = try(send.mail(from = 'operations@cleantechsolar.com',
							to = getRecipients("IN-010X","a"),
							subject = subj,
							body = body,
							smtp = list(host.name = "smtp.office365.com", port = 587, 
							user.name = 'shravan.karthik@cleantechsolar.com',passwd = 'CTS&*(789', tls = TRUE),
							authenticate = TRUE,
							send = TRUE,
							debug = F),silent=T)
							counter = counter + 1
		if(class(sendMail)=='try-error' && counter < 6)
		{
			Sys.sleep(60)
			next
		}
		if(counter > 5)
		{
			print('Error in connecting to the mail server')
		}
		break
		}
		print(paste('Errata mail sent '))
		msgbody = paste(subj,"\n Timestamp:",as.character(row[1]),
										"\n Real Pow Tot kW reading:",as.character(row[2]))
		command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',msgbody,'"',' "IN-010X"',sep = "")
		system(command)
		print('Twilio message fired')
		recordTimeMaster("IN-010X","TwilioAlert",as.character(row[1]))
	}
}

stitchFile = function(path,days,pathwrite,erratacheck)
{
  x = 1
	t = try(read.csv(paste(path,days[x],sep="/"),header = T, skip=6,stringsAsFactors=F),silent = T)
  if(length(t)==1)
	{
		print(paste("Skipping",days[x],"Due to err in file"))
		return()
	}
  dataread = t
	if(nrow(dataread) < 1)
		return()
  dataread = dataread[,-c(1,2)]
  currcolnames = colnames(dataread)
  idxvals = match(currcolnames,colnames)
  for(y in 1 : nrow(dataread))
  {
		idxts = timetomins(substr(as.character(dataread[y,1]),12,16))
    if(!is.finite(idxts))
    {
      print(paste('Skipping row',y,'as incorrect timestamp'))
      next
    }
    rowtemp2 = rowtemp
    yr = substr(as.character(dataread[y,1]),1,4)
    pathwriteyr = paste(pathwrite,yr,sep="/")
    checkdir(pathwriteyr)
    mon = substr(as.character(dataread[y,1]),1,7)
    pathwritemon = paste(pathwriteyr,mon,sep="/")
    checkdir(pathwritemon)
    day = substr(as.character(dataread[y,1]),1,10)
    filename = paste(stnname,"] ",day,".txt",sep="")
    pathtowrite = paste(pathwritemon,filename,sep="/")
    rowtemp2[idxvals] = dataread[y,]
  {
    if(!file.exists(pathtowrite))
    {
      df = data.frame(rowtemp2,stringsAsFactors = F)
      colnames(df) = colnames
      if(idxts != 1)
        {
          df[idxts,] = rowtemp2
          df[1,] = rowtemp
        }
				FIREERRATA <<- 1
				FIRETWILIOCOUNT <<- 0
    }
    else
    {
      df = read.table(pathtowrite,header =T,sep = "\t",stringsAsFactors=F)
      df[idxts,] = rowtemp2
    }
  }
	if(erratacheck != 0)
	{
	  pass = c(as.character(df[idxts,1]),as.character(df[idxts,9]))
	  checkErrata(pass,idxts)
	}
	df = df[complete.cases(df[,1]),]
	tdx = as.POSIXct(strptime(as.character(df[,1]), "%Y-%m-%d %H:%M:%S"))
	df = df[order(tdx),]
  write.table(df,file = pathtowrite,row.names = F,col.names = T,sep = "\t",append = F)
}
	recordTimeMaster("IN-010X","Gen1",as.character(df[nrow(df),1]))
}
path = '/home/admin/Data/Episcript-CEC/EGX IN-010 ALP Delhi'
pathwrite = '/home/admin/Dropbox/Gen 1 Data/[IN-010X]'
pathdatelastread = '/home/admin/Start'
checkdir(pathdatelastread)
pathdatelastread = paste(pathdatelastread,'Delhi.txt',sep="/")
days = dir(path)
days = days[grepl("csv",days)]
{
	if(!file.exists(pathdatelastread))
	{
	  print('Last date read file not found so cleansing system from start')
		system('rm -R "/home/admin/Dropbox/Gen 1 Data/[IN-010X]"')
		idxtostart = 1
	}
	else
	{
		lastrecordeddate = readLines(pathdatelastread)
		lastrecordeddate = as.character(lastrecordeddate[1])
		idxtostart = match(lastrecordeddate,days)
		print(paste('Date read is',lastrecordeddate,'and idxtostart is',idxtostart))
	}
}
checkdir(pathwrite)

colnames = colnames(read.csv(paste(path,days[(length(days)-1)],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnames = colnames[-c(1,2)]
rowtemp = rep(NA,(length(colnames)))
x=1
stnname =  "[IN-010X"
{
	if(idxtostart < length(days))
	{
		print(paste('idxtostart is',idxtostart,'while length of days is',length(days)))
		for(x in idxtostart : length(days))
		{
			stitchFile(path,days[x],pathwrite,0)
		}
		lastrecordeddate = as.character(days[x])
	}
	else
	{
		print('No historical backlog')
	}
}
print('_________________________________________')
print('Historical Calculations done')
print('_________________________________________')
reprint = 1
write(lastrecordeddate,file =pathdatelastread)

while(1)
{
	print('Checking FTP for new data')
 	filefetch = dumpftp(days,path)
	if(filefetch == 0)
	{
		Sys.sleep(600)
		next
	}
	print('FTP Check complete')
  daysnew = dir(path)
	daysnew = daysnew[grepl("csv",daysnew)]
	if(length(daysnew) == length(days))
	{
		if(reprint == 1)
		{
			reprint = 0
			print('Waiting for new file dump')
		}
		Sys.sleep(600)
		next
	}
	reprint = 1
	match = match(days,daysnew)
	days = daysnew
	daysnew = daysnew[-match]
	for(x in 1 : length(daysnew))
	{
		print(paste('Processing',daysnew[x]))
		stitchFile(path,daysnew[x],pathwrite,1)
		print(paste('Done',daysnew[x]))
	}
	write(as.character(daysnew[x]),pathdatelastread)
	gc()
}
print('Exit Loop')
sink()
