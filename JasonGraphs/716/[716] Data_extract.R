rm(list=ls(all =TRUE))

pathRead <- "~/intern/data/[716]"

pathwritetxt <- "C:/Users/talki/Desktop/cec intern/results/716/[716]_wm_summary.txt"
pathwritecsv <- "C:/Users/talki/Desktop/cec intern/results/716/[716]_wm_summary.csv"

setwd(pathRead)
filelist <- dir(pattern = ".txt", recursive= TRUE)
timemin <- format(as.POSIXct("2017-01-07 06:59:59"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2017-09-12 17:00:00"), format="%H:%M:%S")

stationLimit <- 1628.2
nameofStation <- "SG-003A"

col0 <- c()
col1 <- c()
col2 <- c()
col3 <- c()
col4 <- c()
col5 <- c()
col6 <- c()
col0[10^6] = col1[10^6] = col2[10^6] = col3[10^6] = col4[10^6] = col5[10^6] = col6[10^6] = 0
index <- 1

for (i in filelist){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")
  date <- substr(paste(temp[1,1]),1,10)
  col0[index] <- nameofStation
  col1[index] <- date
  
  #data availability
  pts <- length(temp[,1])
  col3[index] <- pts
  
  #gsi
  gsi <- as.numeric(temp[,3])
  col4[index] <- sum(gsi)/60000
  
  #hamb&tamb
  tamb <- mean(as.numeric(temp[,4]))
  col5[index] <- tamb
  hamb <- mean(as.numeric(temp[,5]))
  col6[index] <- hamb

  ex_proc <- index/length(filelist)*100
  print(paste(i, " done"))
  index <- index + 1
}

col0 <- col0[1:(index-1)]
col1 <- col1[1:(index-1)]
col2 <- col2[1:(index-1)]
col3 <- col3[1:(index-1)]
col4 <- col4[1:(index-1)]
col5 <- col5[1:(index-1)]
col6 <- col6[1:(index-1)]
col2 <- col3/max(col3)*100
col2[is.na(col2)] <- NA
col3[is.na(col3)] <- NA
col4[is.na(col4)] <- NA
col5[is.na(col5)] <- NA
col6[is.na(col6)] <- NA

result <- cbind(col0,col1,col2,col3,col4,col5,col6)
colnames(result) <- c("Meter Reference","Date","Data Availability [%]",
                      "No. of Points","GSI [W/m^2]","Tamb [C]","Hamb [%]")
result[,3] <- round(as.numeric(result[,3]),1)
result[,5] <- round(as.numeric(result[,5]),1)
result[,6] <- round(as.numeric(result[,6]),1)
result[,7] <- round(as.numeric(result[,7]),1)

rownames(result) <- NULL
result <- data.frame(result)
result <- result[-length(result[,1]),]
dataWrite <- result
write.table(result,na = "",pathwritetxt,row.names = FALSE,sep ="\t")
write.csv(result,pathwritecsv, na= "", row.names = FALSE)

