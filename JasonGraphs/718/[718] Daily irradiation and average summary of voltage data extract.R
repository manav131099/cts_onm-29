rm(list=ls(all =TRUE))
#
site <- "718"
#

pathRead <- paste0("/home/admin/Dropbox/Cleantechsolar/1min/[",site,"]/")

setwd(pathRead)

pathwritetxt <- paste0("/home/admin/Jason/cec intern/results/",site,"/",site,"_dailysummary.txt")

#functions
rf = function(x)
{
  return(format(round(as.numeric(x),3),nsmall=3))
}

avgsum = function(z)
{
  s1 <- c(mean(z,na.rm=TRUE), max(z,na.rm=TRUE), min(z,na.rm=TRUE))
  if(s1[1] == "NaN")
  {
    s1[1] = s1[2] = s1[3] = "N/A"
  }
  return(s1)
}

filelist <- dir(pattern = ".txt", recursive= TRUE)

#columns of interest; coi for mean/max/min; coim for mean
coim1 <- 3
coim2 <- 4
coim3 <- 5
coim4 <- 6
coim5 <- 8

coi1 <- 18
coi2 <- 19
coi3 <- 20 

coi4 <- 65
coi5 <- 66
coi6 <- 67

df=NULL

for(i in filelist)
{
  temp <- read.table(i,header = T, sep = "\t")
  date <- substr(i,20,29)
  
  di_1 <- rf(sum(temp[,coim1])/60000)
  di_2 <- rf(sum(temp[,coim2])/60000)
  di_3 <- rf(sum(temp[,coim3])/60000)
  di_4 <- rf(sum(temp[,coim4])/60000)
  di_5 <- rf(sum(temp[,coim5])/60000)
  
  a <- t(avgsum(temp[,coi1]))
  b <- t(avgsum(temp[,coi2]))
  c <- t(avgsum(temp[,coi3]))
  
  d <- t(avgsum(temp[,coi4]))
  e <- t(avgsum(temp[,coi5]))
  f <- t(avgsum(temp[,coi6]))
  
  compiled.row <- t(c(date, di_1, di_2, di_3, di_4, di_5, a, b, c, d, e, f))
  df <- rbind(df,compiled.row)
  
  print(paste0(i, ' done'))
}

df <- as.data.frame(df)
df <- df[1:(length(df[,1])-1),]
colnames(df) <- c("Date", 
                  paste0("Irradiation_",colnames(temp[coim1])), 
                  paste0("Irradiation_",colnames(temp[coim2])), 
                  paste0("Irradiation_",colnames(temp[coim3])),
                  paste0("Irradiation_",colnames(temp[coim4])),
                  paste0("Irradiation_",colnames(temp[coim5])),
                  paste0("AVG_",colnames(temp[coi1])), paste0("MAX_",colnames(temp[coi1])), paste0("MIN_",colnames(temp[coi1])),
                  paste0("AVG_",colnames(temp[coi2])), paste0("MAX_",colnames(temp[coi2])), paste0("MIN_",colnames(temp[coi2])),
                  paste0("AVG_",colnames(temp[coi3])), paste0("MAX_",colnames(temp[coi3])), paste0("MIN_",colnames(temp[coi3])),
                  paste0("AVG_",colnames(temp[coi4])), paste0("MAX_",colnames(temp[coi4])), paste0("MIN_",colnames(temp[coi4])),
                  paste0("AVG_",colnames(temp[coi5])), paste0("MAX_",colnames(temp[coi5])), paste0("MIN_",colnames(temp[coi5])),
                  paste0("AVG_",colnames(temp[coi6])), paste0("MAX_",colnames(temp[coi6])), paste0("MIN_",colnames(temp[coi6])))

write.table(df, na = "", pathwritetxt, row.names = FALSE, sep = "\t")

