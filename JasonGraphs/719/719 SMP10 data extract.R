rm(list=ls(all =TRUE))

setwd("~/intern/Data/[719]")
satellite_data <- "~/intern/Data/CEBU Aggregate.txt"  
filelist <- dir(pattern = ".txt", recursive= TRUE)
filelist <- filelist[33:(length(filelist)-1)]
result <- NULL
timemin <- format(as.POSIXct("2016-09-17 05:59:59"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2016-09-17 20:00:00"), format="%H:%M:%S")
for (i in filelist){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")
  date <- substr(i,20,29)
  gsi <- as.numeric(paste(temp[,3]))
  condition <- format(as.POSIXct(temp[,1]), format="%H:%M:%S") > timemin &
    format(as.POSIXct(temp[,1]), format="%H:%M:%S") <timemax
  pacTemp <- temp[condition,]
  data[1] <- date
  #data[2] <- round(length(pacTemp[,1])/14.4,1)#8.4
  data[2] <- round(length(temp[,1])/14.4,1)
  data[3] <- round(sum(gsi)/60000,2)
  result <- rbind(result,data)
  print(paste(i, "done"))
}
colnames(result) <- c("date","da","smp10")
rownames(result) <- NULL
result <- data.frame(result)

result2 <- read.table(satellite_data,header = T)
result2 <- result2[-(1:423),]
result2 <- result2[-74,]    #2018-02-09
result <- result[1:length(result2[,1]),]
gsi <- result2[,2]
result <- cbind(result,gsi)

month <- substr(paste(result[,1]),3,7)
result <- cbind(result,month)
colnames(result) <- c("date","da","measure","model","Month")
rownames(result) <- NULL
result[,2] <- as.numeric(paste(result[,2]))
result[,3] <- as.numeric(paste(result[,3]))
result[,4] <- as.numeric(paste(result[,4]))
result[,5] <- factor(paste(result[,5]))
write.table(result,"C:/Users/talki/Desktop/cec intern/results/719/[719]_smp10_summary.txt",sep = "\t",row.names = F)
