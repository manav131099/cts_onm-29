rm(list=ls(all =TRUE))
require('compiler')
enableJIT(3)
library(ggplot2)
library(gridExtra)
library(gtable)
library(zoo)
ma <- function(x,y,del)
{
  v1 <- zoo(x, as.Date(y))
  v2 <- rollapplyr(v1, list(-(del:1)), mean, fill = NA, na.rm = TRUE)
  return(v2)
}

pathRead <- "/home/admin/Dropbox/Cleantechsolar/1min/[715]"
pathWrite <- "/home/admin/Jason/cec intern/results/KH-003X/"

setwd(pathRead)

filelist <- dir(pattern = ".txt", recursive= TRUE)


col1 <- c()
col2 <- c()
col3 <- c()
col4 <- c()
col1[10^6] = col2[10^6] = col3[10^6] =col4[10^6] =  0
index <- 1

for (i in filelist[-(1:28)]){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")
  date <- substr(paste(temp[1,1]),1,10)
  col1[index] <- date
  
  powerGen <- as.numeric(temp[,24])
  powerRec <- as.numeric(temp[,71])
  powerGen <- sum(powerGen[!is.na(powerGen)])/60
  powerRec <- sum(powerRec[!is.na(powerRec)])/60
  m1diff <- powerGen - powerRec
  
  m1cableloss <- m1diff/powerGen*100    #calculated via power generated and received
  
  col2[index] <- m1cableloss
  
  energyGen <- as.numeric(temp[,39])
  energyRec <- as.numeric(temp[,86])
  energyGen <- -energyGen[1] + energyGen[length(energyGen)]
  energyRec <- -energyRec[1] + energyRec[length(energyRec)]
  m2diff <- energyGen - energyRec
  
  m2cableloss <- m2diff/energyGen*100 #calculated via energy generated and received
  
  col3[index] <- m2cableloss
  
  gsi <- as.numeric(paste(temp[,3]))
  col4[index] <- sum(gsi)/60000
  
  print(paste0(i, " done"))
  index <- index + 1
}

col1 <- col1[1:(index-1)]
col2 <- col2[1:(index-1)]
col3 <- col3[1:(index-1)]
col4 <- col4[1:(index-1)]
col2[col2<0] <- NA

result <- cbind(col1,col2,col3,col4)
result <- data.frame(result)
result[,1] <- as.Date(result[,1], origin = result[,1][1])
result[,2] <- as.numeric(paste(result[,2]))
result[,3] <- as.numeric(paste(result[,3]))
result[,4] <- as.numeric(paste(result[,4]))
result[,5] <- numeric(length(result[,4]))

#grouping
result[,5][result[,4]<2] <- "GSI < 2"
result[,5][result[,4]>2 & result[,4]<4.5] <- "2 < GSI < 4.5"
result[,5][result[,4]>4.5] <- "GSI > 4.5"
result[,5] <- factor(result[,5],levels = c("GSI < 2","2 < GSI < 4.5", "GSI > 4.5"))

result[,2][result[,2]>2] <- NA

mytable <- table(result[,5])

colnames(result) <- c("date","cablem1","cablem2","GSI","Ggroup")
date <- result[,1]
datemin <- date[1]
datemax <- date[length(date)]
setwd(pathWrite)

clgraph <- ggplot(result, aes(x=date,y=cablem1))+ ylab("Cable Loss [%]")
clFinalGraph<- clgraph + geom_point(size=0.5)+
  theme_bw()+
  expand_limits(x=date[1],y=0)+
  scale_y_continuous(breaks=seq(0, 3, 0.5))+
  scale_x_date(date_breaks = "3 month",date_labels = "%b")+
  ggtitle(paste0("[KH-003] Cable Loss-Method 1 (PAC)"), subtitle = paste0("From ",date[1]," to ",date[length(date)])) +
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_blank())+
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5), plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.9,hjust = 0.5))+
  theme(plot.title = element_text(margin=margin(0,0,7,0)))+
  annotate("text",label = paste0("Average cable loss = ", round(mean(result[,2][!is.na(result[,2])]),2),"(lifetime)   "),size=4,
           x = as.Date(date[round(0.518*length(date))]), y= 2.2)+
  annotate("text",label = paste0("Lifetime = ", length(date)," days (",format(round(length(date)/365,1),nsmall = 1)," years)"),size = 4,
           x = as.Date(date[round(0.518*length(date))]), y= 2.35)+
  geom_hline(yintercept=mean(result[,2][!is.na(result[,2])]),size=0.3)+
  
  theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

clFinalGraph
ggsave(clFinalGraph,filename = paste0(pathWrite,"KH-003_CableLoss1.pdf"),width =7.92, height =5 )

print("Graph 1 done")
result[,3][abs(result[,3])>5] <- NA
ma1 <- ma(result[,3], result[,1], 365)
result$mov_avg =  coredata(ma1)

clgraph2 <- ggplot(result)+ylab("Cable Loss [%]")
clFinalGraph2<- clgraph2 + geom_point(aes(x=date,y=cablem2), size=0.5)+
  theme_bw() + geom_line(aes(x=date, y=mov_avg), size =1, colour = "red") +
  expand_limits(x=date[1],y=0) + coord_cartesian(ylim = c(0,1)) +
  scale_y_continuous(breaks=seq(-0, 1, 0.2))+
  scale_x_date(date_breaks = "3 month",date_labels = "%b")+
  ggtitle(paste0("[KH-003] Cable Loss-Method 2 (EAC)"), subtitle = paste0("From ",date[1]," to ",date[length(date)])) +
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_blank())+
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5), plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.9,hjust = 0.5))+
  theme(plot.title=element_text(margin=margin(0,0,7,0)))+
  annotate("text",label = paste0("Average cable loss = ", round(mean(result[length(result[,3]-365):length(result[,3]) ,3], na.rm = TRUE),2),"%  (1 year)    "),size = 4,
           x = as.Date(date[round(0.518*length(date))]), y= 0.86) +
  annotate("text",label = paste0("Average cable loss = ", round(mean(result[,3][!is.na(result[,3])], na.rm = T),2),"%  (lifetime)    "),size = 4,
           x = as.Date(date[round(0.518*length(date))]), y= 0.91) + 
  annotate("text",label = paste0("Lifetime = ", length(date)," days (",format(round(length(date)/365,1),nsmall = 1)," years)"),size = 4,
           x = as.Date(date[round(0.518*length(date))]), y= 0.96) +
  geom_hline(yintercept=mean(result[,3][!is.na(result[,3])]),size=0.3)+
  
  theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

clFinalGraph2

ggsave(clFinalGraph2,filename = paste0(pathWrite,"KH-003_CableLoss2.pdf"),width =7.92, height =5 )
print("Graph 2 done")
#tt3 not working as expected // aes for the gtable
tt3 <- ttheme_minimal(
  core=list(
    bg_params = list(fill = c(rep(c("grey95", "grey90")))),
    fg_params=list(fontface=c(rep("plain",4), "bold.italic"))))
#colhead=list(fg_params=list(col="gray", fontface=4L)),
#rowhead=list(fg_params=list(col="black", fontface=3L)))


print("Base plot start")
#gsigraph <- ggplot(result, aes(x=date,y= GSI,shape = Ggroup)) + ylab("GSI [kWh/m�]")
gsigraph <- ggplot(result,aes(x=date,y=GSI,shape=Ggroup)) + ylab("GSI [kWh/m2]")
print("Started gsigraph")
final_graph3 <- gsigraph + 
  geom_point(size=1,aes(color = Ggroup))+
  theme_bw()+
  scale_y_continuous(breaks = seq(0,8,1))+
  coord_cartesian(ylim=c(0,8.5))+
  scale_x_date(date_breaks = "3 month",date_labels = "%b")+
  ggtitle(paste0("[KH-003] GSI Filter"), subtitle = paste0("From ",date[1]," to ",date[length(date)])) +
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_blank())+
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5), plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.9,hjust = 0.5))+
  theme(plot.title=element_text(margin=margin(0,0,7,0)))+
  annotate("text",label = paste0("Average GSI = ", round(mean(result[,4][result[,4]>0&!is.na(result[,4])]),1),"kWh/m2"), size=4,
           x = as.Date(date[round(0.478*length(date))]), y= 1.2)+  
  annotation_custom(tableGrob(as.matrix(mytable),theme=tt3),xmin = as.numeric(datemax)-73,xmax=as.numeric(datemax)-25,ymin=0.5,ymax= 0.95)+
  geom_hline(yintercept=mean(result[,4][result[,4]>0&!is.na(result[,4])]), size=0.3)+
  
  theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

final_graph3

ggsave(final_graph3, filename = paste0(pathWrite,"irradiance filter plot 2.pdf"), width =7.92, height = 5 )
print("Graph 3 done")


#cable losses text file
write.table(result,file = paste(pathWrite,"KH-003_Cableloss.txt",sep=""),sep = "\t",row.names = FALSE)
