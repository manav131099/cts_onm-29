rm(list=ls(all =TRUE))
library(ggplot2)
args <- commandArgs(TRUE)
yr = as.character(args[1])
mon = as.character(args[2])
pathRead <- paste("/home/admin/Dropbox/Gen 1 Data/[TH-003X]/",yr,"/",mon,"/MDB-01 Invoice",sep="")     #SCSC-S1
pathRead2 <- paste("/home/admin/Dropbox/Gen 1 Data/[TH-003X]/",yr,"/",mon,"/MDB-02 Invoice",sep="")     #SCSC-S2
setwd(pathRead)
date = as.character(Sys.Date())
writetxt <- paste('/tmp/[TH-003X] TH Invoicing summary ',date,'.txt',sep="")
filelist <- dir(pattern = ".txt", recursive= TRUE)
setwd(pathRead2)
filelist2 <-dir(pattern = ".txt",recursive=T)
setwd(pathRead)

rf = function(x){
  return(format(round(x,2),nsmall=2))
}

coerce_num <- function(x)
{
  v1 <- as.numeric(as.character(x))
  return(v1)
}

#Peak condition 1 
#Condition 1; defining peak time
timemin <- format(as.POSIXct("2017-01-07 08:59:00"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2017-01-07 22:01:00"), format="%H:%M:%S")

#Public holidays obs in 2018 APRIL [Thailand] 
#Condition 2; defining off peak days on week days)
##https://www.officeholidays.com/countries/thailand/index.php
	PH = c(
	as.Date("2018-04-06"),    #Chakri Day
	as.Date("2018-04-12"),    #Songkran 4
	as.Date("2018-04-13"),    #Songkran
	as.Date("2018-04-14"),    #Songkran 2
	as.Date("2018-04-15"),    #Songkran 3
	as.Date("2018-04-16"),    #Songkran obs
	as.Date("2018-05-01"),    #Labour day
	as.Date("2018-05-14"),    #Royal ploughing ceremony
	as.Date("2018-05-29"),    #Visakha Bucha
	as.Date("2018-07-27"),   #Asaalha Bucha
	as.Date("2018-08-12"),   #Queen's bday
	as.Date("2018-08-13"),   #Queen's bday obs
	as.Date("2018-10-13"),   #Anniversary of Death of King Bhumibol
	as.Date("2018-10-15"),   #Anniversary of Death of King Bhumibol obs
	as.Date("2018-10-23"),   #Chulalongkorn Day
	as.Date("2018-12-05"),   #Father's day
 	as.Date("2018-12-10"),   #Constitution Day
	as.Date("2018-12-31"),
	as.Date("2019-01-01"),
	as.Date("2019-02-19"),
	as.Date("2019-04-15"),
	as.Date("2019-05-01"),
	as.Date("2019-07-16"),
	as.Date("2019-07-17"),
	as.Date("2019-08-12"),
	as.Date("2019-10-23"),
	as.Date("2019-12-05"),
	as.Date("2019-12-10"),
	as.Date("2019-12-31"),
    as.Date("2020-01-01"),
	as.Date("2020-04-06"),
	as.Date("2020-05-01"),
	as.Date("2020-05-04"),
	as.Date("2020-05-06"),
	as.Date("2020-06-03"),
	as.Date("2020-07-06"),
	as.Date("2020-07-28"),
	as.Date("2020-08-12"),
	as.Date("2020-10-13"),
	as.Date("2020-10-23"),
	as.Date("2020-12-10"),
	as.Date("2020-12-31")
)   #New year's eve

#dates <- seq.Date(as.Date("2017-01-01"),as.Date("2017-12-31"),by="1 day")

i=0
df=dff=NULL
lenac = min(length(filelist),length(filelist2))
filelist = tail(filelist,n=lenac)
filelist2 = tail(filelist2,n=lenac)

for(z in 1:length(filelist)){
	print(z)
  temp <- read.table(paste(pathRead,filelist[z],sep="/"), header = T, sep= '\t', stringsAsFactors = F)
  tempm2 <- read.table(paste(pathRead2,filelist2[z],sep="/"), header = T, sep= '\t', stringsAsFactors = F)
  #data availability
  da <- (length(temp[,1])/1440)*100
  dam2 <- (length(tempm2[,1])/1440)*100
  condition0 <- format(as.POSIXct(temp[,1]), format="%H:%M:%S") > format(as.POSIXct("2017-01-07 05:59:00"), format="%H:%M:%S") & 
    format(as.POSIXct(temp[,1]), format="%H:%M:%S") < format(as.POSIXct("2017-01-07 20:00:00"), format="%H:%M:%S")  #for DA
  condition0m2 <- format(as.POSIXct(tempm2[,1]), format="%H:%M:%S") > format(as.POSIXct("2017-01-07 05:59:00"), format="%H:%M:%S") & 
    format(as.POSIXct(tempm2[,1]), format="%H:%M:%S") < format(as.POSIXct("2017-01-07 20:00:00"), format="%H:%M:%S")  #for DA
  temp2 <- temp[condition0,]
  temp2m2 <- tempm2[condition0m2,]
  da_2 <- (length(temp2[,1])/840)*100
  da_2m2 <- (length(tempm2[,1])/840)*100
  condition1 <- format(as.POSIXct(temp[,1]), format="%H:%M:%S") > timemin & format(as.POSIXct(temp[,1]), format="%H:%M:%S") <timemax
  temp[,15][temp[,15] < 0] <- 0
  condition1m2 <- format(as.POSIXct(tempm2[,1]), format="%H:%M:%S") > timemin & format(as.POSIXct(tempm2[,1]), format="%H:%M:%S") <timemax
  tempm2[,15][tempm2[,15] < 0] <- 0
  pac_TH <- coerce_num(temp[,15])
  pac_THm2 <- coerce_num(tempm2[,15])
  total_eac <- as.numeric(sum(pac_TH, na.rm = T)/60000)          #Eac here appears as kWh
  total_eacm2 <- as.numeric(sum(pac_THm2, na.rm = T)/60000)          #Eac here appears as kWh
  pacTemp <- temp[condition1,]
  pacTempm2 <- temp[condition1m2,]
  date=as.Date(temp[1,1])
	if(is.finite(match(date,PH)))
  {
    peak_eac = 0
    nonpeak_eac= total_eac
    peak_eacm2 = 0
    nonpeak_eacm2 = total_eac
    holi <- "Y"
  } 
  else{
    pac_TH2 <- coerce_num(pacTemp[,15])
    peak_eac <- as.numeric(rf(sum(pac_TH2, na.rm = T)/60000))
    nonpeak_eac = total_eac - peak_eac
    pac_TH2m2 <- coerce_num(pacTempm2[,15])
    peak_eacm2 <- as.numeric(rf(sum(pac_TH2m2, na.rm = T)/60000))
    nonpeak_eacm2 = total_eacm2 - peak_eacm2
    holi <- "N"
  }
  d <- format(head(date), format="%w")
  day <- format(date, format = "%a")
  #Condition 3 ; weekends = Off Peak
  #defining non peak period at 0 (sunday) and 6 (saturday)
  if(d == 0 | d == 6) {
    peak_eac = 0
    nonpeak_eac = total_eac
    peak_eacm2 = 0
    nonpeak_eacm2 = total_eacm2
  }
  #powertotal column BW ; 71
  #percentages
  pk_eac_per <- (peak_eac/total_eac)*100
  #offpk_eac_per <- (nonpeak_eac/total_eac)*100
  offpk_eac_per <- 100 - pk_eac_per
  start_read <- as.numeric(temp[1,39])
  last_read <- as.numeric(temp[length(temp[,1]),39])   # in Wh
  total_eac_m2 <-  last_read-start_read
  
	pk_eac_perm2 <- (peak_eacm2/total_eacm2)*100
  offpk_eac_perm2 <- (nonpeak_eacm2/total_eacm2)*100
  start_readm2 <- as.numeric(tempm2[1,39])
  last_readm2 <- as.numeric(tempm2[length(tempm2[,1]),39])   # in Wh
  total_eac_m2m2 <-  last_readm2-start_readm2
  
df <- cbind(as.character(date), 
              day, 
              holi, 
              rf(da), rf(da_2),
              rf(dam2),
              last_read/1000, total_eac, total_eac_m2/1000,
              last_readm2/1000,
              total_eacm2, total_eac_m2m2/1000,
              rf(peak_eac), rf(nonpeak_eac),
              rf(peak_eacm2), rf(nonpeak_eacm2),
              rf(pk_eac_per), rf(offpk_eac_per),
              rf(pk_eac_perm2), rf(offpk_eac_perm2))            
  
  dff <- rbind(dff,df)
  print(paste(z, " done"))
}

dff=data.frame(dff)

#colnames(dff) <- c("Date","DA-M1 (%)", "DA_6AM_TO_8PM (%)" ,"Eac1-M1 (kWh)", "Eac2-M1 (kWh)","Peak_Eac-M1 (kWh)","Offpeak_Eac-M1 (kWh)", "Peak_Eac-M1 (%)", "Offpeak_Eac-M1 (%)", "Eac delivered-M1 (kWh)", "DA-M2 (%)", "DA_6AM_TO_8PM-M2 (%)" ,"Eac1-M2 (kWh)", "Eac2-M2 (kWh)","Peak_Eac-M2 (kWh)","Offpeak_Eac-M2 (kWh)", "Peak_Eac-M2 (%)", "Offpeak_Eac-M2 (%)", "Eac delivered-M2 (kWh)", "Day" ,"Holiday (Y/N)")
colnames(dff) <- c("Date","Day", "Holiday (Y/N)" ,
                    "DA-M1 (%)", "DA-M2 (%)","DA_6AM_TO_8AM (%)", "Eac Delivered (M1)","Eac1-M1 (kWh)" ,"Eac2-M1 (kWh)", "Eac Delivered (M2)", "Eac1-M2 (kWh)", 
                    "Eac2-M2 (kWh)", "Peak_Eac-M1 (kWh)" ,
                    "Peak_Eac-M2 (kWh)", "Offpeak_Eac-M1 (kWh)","Offpeak Eac-M2 (kWh)","Peak_Eac-M1 (%)", "Peak_Eac-M2 (%)", "Offpeak_Eac-M1 (%)", "Offpeak_Eac-M2 (%)")


write.table(dff,writetxt, row.names = FALSE, sep= "\t")

#themesettings1 <- theme(plot.title = element_text(hjust = 0.5), axis.title.x = element_text(size=19), axis.title.y = element_text(size=19), 
#                        axis.text = element_text(size=11), 
#                        panel.grid.minor = element_blank(), panel.border = element_blank(),axis.line = element_line(colour = "black"),
#                        panel.grid.major.x = element_blank(),
#                        legend.justification = c(1, 1), legend.position = c(1, 1))

#dff$Peak_Eac <- as.numeric(as.character(dff$Peak_Eac))
#dff$Offpeak_Eac <- as.numeric(as.character(dff$Offpeak_Eac))
#dff$Eac1 <- as.numeric(as.character(dff$Eac1))

#dff[,7] <- as.numeric(as.character(dff[,7]))
#dff[,8] <- as.numeric(as.character(dff[,8]))

#p1 <- ggplot() + theme_bw()
#p1 <- p1 + geom_line(aes(x= as.Date(Date), y = Total_Eac, colour = as.factor("orange2"))) #total eac
#p1 <- p1 + geom_line(data=dff, aes(x= as.Date(Date), y = Offpeak_Eac, colour = "Off-Peak")) #Off peak eac
#p1 <- p1 + geom_line(data=dff, aes(x= as.Date(Date), y = Peak_Eac, colour = "Peak"))  #peak eac
#p1 <- p1 + themesettings1 + coord_cartesian(ylim = c(0,8000)) + ylab('Eac [kWh/m�]') + xlab('Date')
#p1 <- p1 + scale_x_date(date_breaks = "5 day",date_labels = "%b %d")
#p1 <- p1 + scale_colour_manual('Period', values = c("Peak"="red", "Off-Peak"="lightblue")) #,labels = c('Peak EAC','Off-Peak EAC')) 

#p1
#ggsave(paste0('~/intern/Results/[TH-001X]/1. result/TH EAC 2018-04 real th.pdf'), p1, width =12, height=6)



