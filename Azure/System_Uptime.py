import requests, json
import requests.auth
import pandas as pd
import datetime 
import os
import re 
import time
import pyodbc
import pytz
import sys
import numpy as np


path='/home/admin/Dropbox/Gen 1 Data/'
stn=['IN-023','IN-037','IN-041','IN-045','IN-047','IN-049','IN-051','IN-053','IN-055','IN-056','IN-057','IN-058','IN-059','IN-060','IN-062','IN-065','IN-066','IN-067','IN-068','IN-069','IN-070','IN-072','IN-074','IN-075','IN-076','IN-077','IN-078','IN-080','IN-081','IN-082','IN-083','IN-085','IN-086','IN-087','IN-088','IN-089','IN-090','IN-091','IN-092','IN-301','IN-302','IN-304','MY-010','MY-401','MY-402','MY-403','MY-404','MY-405','MY-406','MY-407','MY-408','MY-409','MY-410','MY-411','MY-412','MY-413','MY-414','MY-420','MY-425','MY-435','MY-448','TH-010','TH-011','TH-012','TH-013','TH-014','TH-015','TH-016','TH-017','TH-018','TH-019','TH-020','TH-021','TH-022','TH-023','TH-024','TH-025','TH-026','TH-027','TH-028']
tz = pytz.timezone('Asia/Kuala_Lumpur')

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'

def calc_ga_pa_da_sh(mfm_df,date,stn_id,meter_id,country_id,mask):
    mfm_df=mfm_df[mask]
    freq_timestamps=mfm_df.loc[mfm_df['Hz_avg']>40,'ts']
    power_timestamps=mfm_df.loc[mfm_df['W_avg']>2,'ts']
    pa_common=list(set(freq_timestamps) & set(power_timestamps))
    GA=(float(len(freq_timestamps))/float(len(mfm_df['ts'])))*100
    PA=(float(len(pa_common))/float(len(freq_timestamps)))*100
    DA=(float(len(mfm_df[mfm_df['W_avg'].notnull()]))/120)*100
    SA=round((GA*PA)/100,2)
    if(DA>100):
        DA=100
    azure_push(date,GA,PA,DA,SA,stn_id,meter_id,country_id)

#Daily GA,PA,DA Calculation
def calc_ga_pa_da(date,stn,irr_stn,mfm_name,wms_name,stn_id,meter_id,country_id):
    mfm_no=mfm_name.split('_')[1]
    if(wms_name==''):
        wms_no='1'
    else:
        wms_no=wms_name.split('_')[1]
    mfm_file=path+'['+stn+'L]/'+date[0:4]+'/'+date[0:7]+'/'+mfm_name+'/['+stn+'L]-MFM'+mfm_no+'-'+date+'.txt'
    if(irr_stn=='NULL'):
        wms_file=path+'['+stn+'L]/'+date[0:4]+'/'+date[0:7]+'/'+wms_name+'/['+stn+'L]-WMS'+wms_no+'-'+date+'.txt'
    else:
        wms_file=path+'['+irr_stn+'L]/'+date[0:4]+'/'+date[0:7]+'/'+wms_name+'/['+irr_stn+'L]-WMS'+wms_no+'-'+date+'.txt'
    if(os.path.exists(mfm_file)):
        mfm_df=pd.read_csv(mfm_file,sep='\t')
        mfm_df['ts']=pd.to_datetime(mfm_df['ts'])
        mask = (mfm_df['ts'].dt.hour > 6) & (mfm_df['ts'].dt.hour < 18)
        mfm_df_sh=mfm_df[mask]
        freq_timestamps=mfm_df.loc[mfm_df['Hz_avg']>40,'ts']
        freq_timestamps_sh=mfm_df_sh.loc[mfm_df_sh['Hz_avg']>40,'ts']
        if(len(freq_timestamps)>0 and len(freq_timestamps_sh)>0):
            if(os.path.exists(wms_file)):
                wms_df=pd.read_csv(wms_file,sep='\t')
                if(len(wms_df)>0 and 'POAI_avg' in wms_df.columns.tolist()):
                    irr_df=wms_df[['ts','POAI_avg']]
                    irr_df=irr_df[irr_df['POAI_avg'].notnull()]
                    mfm_df=mfm_df[['ts','Hz_avg','W_avg']]
                    irr_timestamps=irr_df.loc[irr_df['POAI_avg']>20,'ts']
                    freq_timestamps=mfm_df.loc[mfm_df['Hz_avg']>40,'ts']
                    power_timestamps=mfm_df.loc[mfm_df['W_avg']>2,'ts']
                    ga_common=list(set(freq_timestamps) & set(irr_timestamps))
                    pa_common=list(set(freq_timestamps) & set(irr_timestamps)  & set(power_timestamps))
                    if((len(irr_timestamps)>0) and (len(ga_common)>0)):
                        GA=(float(len(ga_common))/float(len(irr_timestamps)))*100
                        PA=(float(len(pa_common))/float(len(ga_common)))*100
                        DA=(float(len(mfm_df[mfm_df['W_avg'].notnull()]))/288)*100
                        SA=round((GA*PA)/100,2)
                        if(DA>100):
                            DA=100
                        azure_push(date,GA,PA,DA,SA,stn_id,meter_id,country_id)
                    else:
                        calc_ga_pa_da_sh(mfm_df,date,stn_id,meter_id,country_id,mask)
                else:
                    mfm_df=mfm_df[['ts','Hz_avg','W_avg']]
                    calc_ga_pa_da_sh(mfm_df,date,stn_id,meter_id,country_id,mask)
            else:
                mfm_df=mfm_df[['ts','Hz_avg','W_avg']]
                calc_ga_pa_da_sh(mfm_df,date,stn_id,meter_id,country_id,mask)
        else: 
            azure_push(date,"NULL","NULL",0,"NULL",stn_id,meter_id,country_id)
    
    else: 
        print('no file')
        azure_push(date,"NULL","NULL",0,"NULL",stn_id,meter_id,country_id)



def azure_push(date,GA,PA,DA,SA,stn_id,meter_id,country_id):
        cursor = connStr.cursor()
        with cursor.execute("UPDATE [dbo].[System_Uptime] SET [GA] = {},[PA] = {},[DA] = {},[SA] = {} where [Date] = '{}' AND [Meter_Id]={} if @@ROWCOUNT = 0 INSERT into [dbo].[System_Uptime](Date, GA, PA, DA, SA, Station_Id, Meter_Id, Country_Id, [Master-Flag]) values('{}', {}, {}, {}, {}, {}, {}, {}, {}) ".format(str(GA),str(PA),str(DA),str(SA), str(date),str(meter_id), str(date) ,str(GA) ,str(PA) ,str(DA) ,str(SA) ,str(stn_id) ,str(meter_id) ,str(country_id) ,str(0))):
            pass
        connStr.commit()

def find_first_date(stn):
    for i in sorted(os.listdir(path+'['+stn+'L]/')):
        for j in sorted(os.listdir(path+'['+stn+'L]/'+i)):
            for k in sorted(os.listdir(path+'['+stn+'L]/'+i+'/'+j)):
                for l in sorted(os.listdir(path+'['+stn+'L]/'+i+'/'+j+'/'+k)):
                    date=l[-14:-4]
                    break
                break
            break
        break
    return date

#Hisorical
connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
SQL_Query = pd.read_sql_query('''SELECT TOP (1000) [Station_Id],[Station_Name],[Station_Columns],[Station_Irradiation_Center],[Station_No_Meters],[Provider],[Alarm_Status] FROM [dbo].[Stations] ''', connStr)
df_stations = pd.DataFrame(SQL_Query, columns=['Station_Id','Station_Name','Station_Columns','Station_Irradiation_Center','Station_No_Meters','Provider','Alarm_Status'])
SQL_Query = pd.read_sql_query('''SELECT * FROM [dbo].[Meters] ''', connStr)
df_meters = pd.DataFrame(SQL_Query, columns=['Meter_id','Station_Id','Country_Id'])

date_today=datetime.datetime.now(tz).date()

for i in stn:
    print(i)
    df_stations_temp=df_stations.loc[(df_stations['Station_Name'].str.strip()==i ),:]
    n=df_stations_temp['Station_No_Meters'].values[0]
    cols=df_stations_temp['Station_Columns'].values[0]
    irr_stn=df_stations_temp['Station_Irradiation_Center'].fillna('NULL').values[0].strip()
    stn_id=df_stations_temp['Station_Id'].values[0]
    meter_id=df_meters.loc[(df_meters['Station_Id']==stn_id),'Meter_id'].tolist()
    country_id=df_meters.loc[(df_meters['Station_Id']==stn_id),'Country_Id'].tolist()
    SQL_Query = pd.read_sql_query('SELECT TOP (1) [Station_Id] FROM [dbo].[System_Uptime] Where Station_Id='+str(stn_id), connStr)
    dflifetime = pd.DataFrame(SQL_Query, columns=['Station_Id'])
    if(dflifetime.empty):
        temp=[]
        for index,j in enumerate(cols.strip().split(',')):
            if(i=='IN-070'):
                temp=['','MFM_3_ESS-02 Main_MFM', 'MFM_1_ESS-03 Main_MFM', 'MFM_7_ESS-10 Main_MFM']
            elif(i=='MY-010'):
                temp=['','MFM_5_LG1-1 MFM', 'MFM_6_LG1-2 MFM', 'MFM_7_L3B MFM', 'MFM_8_L3A MFM', 'MFM_9_LG1B MFM', 'MFM_11_LG1A MFM']
            elif(i=='IN-037'):
                temp=['WMS_1_WMS','MFM_1_Plant Meter-Tooling Shed Block', 'MFM_2_Plant Meter-Corporate Office']
            elif(i=='TH-024'):
                temp=['WMS_1_Pyranometer','MFM_1_PV MFM-1', 'MFM_2_PV MFM-2']
            else:
                if(index==1):
                    temp2=(j.replace('"','').split('.')[:-1])
                    temp.append(' '.join(temp2))
                if(index>1 and index<(2+n)): #Skip date and include meters only
                    temp2=(j.replace('"','').split('.')[:-1])
                    temp.append(' '.join(temp2))
        wms_name=temp[0]
        mfm_name=temp[1:]
        for index,mfm in enumerate(mfm_name):
            start_date=datetime.datetime.strptime(find_first_date(i),"%Y-%m-%d").date()
            while(start_date<date_today):
                calc_ga_pa_da(str(start_date),i,irr_stn,mfm,wms_name,stn_id,meter_id[index],country_id[index])
                start_date=start_date+datetime.timedelta(days=1)


#Live
while(1):
    connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    SQL_Query = pd.read_sql_query('''SELECT TOP (1000) [Station_Id],[Station_Name],[Station_Columns],[Station_Irradiation_Center],[Station_No_Meters],[Provider],[Alarm_Status] FROM [dbo].[Stations] ''', connStr)
    df_stations = pd.DataFrame(SQL_Query, columns=['Station_Id','Station_Name','Station_Columns','Station_Irradiation_Center','Station_No_Meters','Provider','Alarm_Status'])
    SQL_Query = pd.read_sql_query('''SELECT * FROM [dbo].[Meters] ''', connStr)
    df_meters = pd.DataFrame(SQL_Query, columns=['Meter_id','Station_Id','Country_Id'])
    for i in range(7,-1,-1):
        date=(datetime.datetime.now(tz)+datetime.timedelta(days=-i)).strftime('%Y-%m-%d')
        print(date)
        for i in stn:
            print(i)
            df_stations_temp=df_stations.loc[(df_stations['Station_Name'].str.strip()==i ),:]
            n=df_stations_temp['Station_No_Meters'].values[0]
            cols=df_stations_temp['Station_Columns'].values[0]
            irr_stn=df_stations_temp['Station_Irradiation_Center'].fillna('NULL').values[0].strip()
            stn_id=df_stations_temp['Station_Id'].values[0]
            meter_id=df_meters.loc[(df_meters['Station_Id']==stn_id),'Meter_id'].tolist()
            country_id=df_meters.loc[(df_meters['Station_Id']==stn_id),'Country_Id'].tolist()
            temp=[]
            for index,j in enumerate(cols.strip().split(',')):
                if(i=='IN-070'):
                    temp=['','MFM_3_ESS-02 Main_MFM', 'MFM_1_ESS-03 Main_MFM', 'MFM_7_ESS-10 Main_MFM']
                elif(i=='MY-010'):
                    temp=['','MFM_5_LG1-1 MFM', 'MFM_6_LG1-2 MFM', 'MFM_7_L3B MFM', 'MFM_8_L3A MFM', 'MFM_9_LG1B MFM', 'MFM_11_LG1A MFM']
                elif(i=='IN-037'):
                    temp=['WMS_1_WMS','MFM_1_Plant Meter-Tooling Shed Block', 'MFM_2_Plant Meter-Corporate Office']
                elif(i=='TH-024'):
                    temp=['WMS_1_Pyranometer','MFM_1_PV MFM-1', 'MFM_2_PV MFM-2']
                else:
                    if(index==1):
                        temp2=(j.replace('"','').split('.')[:-1])
                        temp.append(' '.join(temp2))
                    if(index>1 and index<(2+n)): #Skip date and include meters only
                        temp2=(j.replace('"','').split('.')[:-1])
                        temp.append(' '.join(temp2))
            wms_name=temp[0]
            mfm_name=temp[1:]
            for index,mfm in enumerate(mfm_name):
                calc_ga_pa_da(date,i,irr_stn,mfm,wms_name,stn_id,meter_id[index],country_id[index])
    connStr.close()
    time.sleep(3600)