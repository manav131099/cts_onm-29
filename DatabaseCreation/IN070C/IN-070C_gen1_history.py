import urllib.request, json
import csv,time
import datetime
import collections
import os
import re
import pytz
from functools import reduce
#Global declarations
wordDictI = {
'i9':'AC_Voltage_R','i10':'AC_Voltage_Y','i11':'AC_Voltage_B','i12':'AC_Current_R','i13':'AC_Current_Y','i14':'AC_Current_B','i15':'AC_Power',
'i16':'AC_Power_Percentage','i17':'AC_Frequency','i18':'Power_Factor','i19':'Reactive_Power','i20':'DC_Current','i21':'DC_Power',
'i22':'Inverter_Temperature','i23':'Time_Of_Use_today','i24':'Time_Of_Use_life','i26':'KWh_Counter','i27':'MWh_Counter','i28':'GWh_Counter',
'i29':'DC_Voltage','i30':'Inverter_Status','i31':'Todays_Energy','i32':'Tstamp','i33':'Total_Energy','i34':'Inverter_Communication_Status',
'i35':'AC_Power_2','i36':'AC_Power_3','i37':'AC_Frequency_2','i38':'AC_Frequency_3','i39':'DC_Voltage_2','i40':'DC_Power_2','i41':'DC_Current_2',
'i42':'Plant_Id','i43':'Inverter_Status_Word','i44':'Grid_Conn_Status','i45':'AC_Current_Total','i46':'AC_Volatge_RY','i47':'AC_Volatge_YB',
'i48':'AC_Volatge_BR','i49':'Apparent_Power','i50':'Event_Flag_1','i51':'Event_Flag_2','i52':'Event_Flag_3','i53':'Event_Flag_4','i54':'Coolent_Temp',
'i55':'DC_Voltage_3','i56':'DC_Current_3','i57':'DC_Voltage_4','i58':'DC_Current_4','i59':'Inverter_Efficiency','i60':'SPD_Status','i61':'Switch_Status',
'i62':'String_1_Current','i63':'String_2_Current','i64':'String_3_Current','i65':'String_4_Current','i66':'String_5_Current','i67':'String_6_Current',
'i68':'String_7_Current','i69':'String_8_Current','i70':'String_9_Current','i71':'String_10_Current','i72':'String_11_Current','i73':'String_12_Current',
'i74':'String_13_Current','i75':'String_14_Current','i76':'String_15_Current','i77':'String_16_Current','i78':'String_17_Current','i79':'String_18_Current',
'i80':'String_19_Current','i81':'String_20_Current','i82':'String_21_Current','i83':'String_22_Current','i84':'String_23_Current','i85':'String_24_Current',
'i87':'String_Current_Avg'
}

wordDictM = {
'm6':'MFM_Identifier','m8':'AC_Voltage_R','m9':'AC_Voltage_Y','m10':'AC_Voltage_B','m11':'AC_Voltage_Avg','m12':'AC_Voltage_RY','m13':'AC_Voltage_YB',
'm14':'AC_Voltage_BR','m15':'AC_VLL_Avg','m16':'AC_Current_R','m17':'AC_Current_Y','m18':'AC_Current_B','m19':'AC_Current_N','m20':'AC_Current_Total',
'm28':'Power_Factor_Avg','m29':'AC_Power_R_Phase','m30':'AC_Power_Y_Phase','m31':'AC_Power_B_Phase','m32':'AC_Power_Total','m34':'Reactive_Power_R_Phase',
'm35':'Reactive_Power_Y_Phase','m36':'Reactive_Power_B_Phase','m37':'Reactive_Power_Total','m38':'Reactive_Power_Avg','m41':'Apparent_Power_R_Phase',
'm42':'Apparent_Power_Y_Phase','m43':'Apparent_Power_B_Phase','m64':'Tstamp','m65':'Active_Import','m66':'Active_Export','m67':'Plant_Id'
 }

wordDictW = {
'w9':'Humidity_Min','w10':'Module_Temp1','w11':'Wind_Direction','w12':'Wind_Speed','w13':'Ambient_Temp','w15':'Humidity_Max',
'w16':'Humidity_Actual','w17':'Ambient_Temp_Min','w18':'Ambient_Temp_Max','w19':'Ambient_Temp_Avg','w20':'Global_Irradiation_Min','w21':'Irradiation_Tilt1_Actual',
'w22':'Irradiation_Tilt2_Actual','w23':'Tstamp','w24':'Global_Irradiation_Max','w25':'Global_Irradiation_Avg','w26':'Wind_Speed_Min','w27':'Wind_Speed_Max',
'w28':'Humidity_Avg','w29':'Wind_Direction_Min','w30':'Wind_Direction_Max','w31':'Wind_Speed_Avg','w32':'Global_Irradiation_Actual','w34':'Rain','w35':'Room_Temperature'
    }




#Function to read start date
def read_date():
##    file = open('F:/Flexi_final/[IN-070C]/Gen-1/IN070CG.txt','r') ###local
    file = open('/home/admin/Start/IN070CG.txt','r') ###server
    date_read = file.read(10)
    return date_read


#Function to set start date
def set_date():
    date_read = read_date()
    global date
    date = datetime.datetime.strptime(date_read, '%Y-%m-%d').date()
    date += datetime.timedelta(days=-1) #To reduce date by a day as it gets added in current_date()

#Function to determine the next day
def determine_date():
    global date
    date += datetime.timedelta(days=1)
    return date

#Function to determine path and create directory 
def determine_path(curr_date,device,fd):
    year = curr_date.strftime("%Y")
    month = curr_date.strftime("%Y-%m")
    day = curr_date.strftime("%Y-%m-%d")
##    master_path = 'F:/del/FlexiMC_Data/Gen1_Data/[IN-070C]/' ###local
    master_path = '/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-070C]/' ###server
    year_path = master_path+year+'/'
    month_path = year_path+month+'/'
    device_path = month_path+device+'/'
    final_path = device_path+'[IN-070C]-'+fd+'-'+day+'.txt'
    
    if not os.path.exists(year_path):
        os.makedirs(year_path)
    if not os.path.exists(month_path):
        os.makedirs(month_path)
    if not os.path.exists(device_path):
        os.makedirs(device_path)
    if os.path.exists(final_path):
        os.remove(final_path)
        print('[IN-070C]-'+fd+'-'+day+'.txt'+' removed')
    
    return final_path

#Function to replace header names 
def multiple_replace(text,name_head):

  if name_head == 'inverter':
      for key in wordDictI:
          text = re.sub(r"\b%s\b" % key, wordDictI[key],text)
      return text

  if name_head == 'MFM':
      for key in wordDictM:
          text = re.sub(r"\b%s\b" % key, wordDictM[key],text)
      return text

  if name_head == 'WMS':
      for key in wordDictW:
          text = re.sub(r"\b%s\b" % key, wordDictW[key],text)
      return text
    

#Function to reorder columns 
def reorder(line,name_head):
    if name_head == 'inverter':
        col = 31
    elif name_head == 'MFM':
        col = 63
    else:
        col = 22    
    
    split = line.split('\t')
    temp = split[0]
    split[0] = split[col]
    split[col] = temp
    join = '\t'.join(split)
    return join


#Function to join average line
def get_avg(start_time,no_col,value,name_head):
    avg_line = str(start_time)
    for i in range(1,no_col):
        avg_line = avg_line + '\t'+str(value[i])
##    if name_head == 'inverter':
##        avg_line = avg_line + '\n'
    avg_line = avg_line + '\n'
    return avg_line



#Function to compute average values
def cmpt_avg(no_col,value,occ,counter):
    for i in range(no_col):
        try:
            value[i] = float(value[i])
            if value[i]!= 0:
                if i == 32:
                    value[i] = value[i]/(occ-counter)
                else:
                    value[i] = value[i]/occ
                    value[i] = round(value[i],7)
        
        except ValueError:
            pass
    return value
      


#Function to rename and reorder files 
def read_write_ops(name_head,read_path,write_path,curr_date):
    with open(read_path, 'r') as read_file, open(write_path, 'a') as write_file :
                print('reading '+ read_path)
                data = read_file.readlines()
                lines = []
                header = 0
                occ = 0
                counter = 0
                
                start_time = datetime.datetime.strptime(str(curr_date), "%Y-%m-%d")
                next_date = curr_date + datetime.timedelta(days=1)  # to get the last minute of day 
                end_time = datetime.datetime.strptime(str(next_date), "%Y-%m-%d")
                for line in data:
                    line = reorder(line,name_head)  
                    if header == 0:
                        line = multiple_replace(line,name_head)
                        lines.append(line)
                        header = 1
                    else:
                        split = line.split('\t')
                        no_col = len(split)
                        timestamp = split[0]
                        timestamp = timestamp[:16] #To remove seconds
                        
                        curr_time = datetime.datetime.strptime(timestamp, "%Y-%m-%d %H:%M")

                        while start_time < curr_time:
                            if occ>0:
                                value = cmpt_avg(no_col,value,occ,counter)
                                if name_head == 'inverter':
                                    if value[32] != 0:
                                        line = get_avg(start_time,no_col,value,name_head)
                                        lines.append(line)
                                    else:
                                        print('Skipping ',str(start_time),' as Total Energy is 0')
                                else:
                                    line = get_avg(start_time,no_col,value,name_head)
                                    lines.append(line)                                    
                                
                            start_time+=datetime.timedelta(minutes=5)
                            occ = 0
                            counter = 0

                        if occ == 0:
                            value = [0]*no_col                        
                        if(curr_time <= start_time):
                            occ +=1
                            for i in range(no_col):
                                try:
                                    if name_head == 'inverter' and i == 32:
                                        if split[i] == '0' or split[i] == 'NA' or split[i] == 'NULL':
                                            counter += 1
                                    value[i] += float(split[i])
                                except ValueError: ##to check if string
                                    value[i] = split[i]
                                except TypeError:
                                    if value[i] == 'NULL' or value[i] == 'NA': ## If NULL/NA is first, then can't do 'value[i] += float(split[i])' operation
                                        value[i] = 0
                                        value[i] += float(split[i])

                ## to add remaining rows (needed for last record as it won't enter 'while start_time < curr_time:' loop as no values present after that)       
                if occ>0 and start_time<end_time: 
                    value = cmpt_avg(no_col,value,occ,counter)
                    if name_head == 'inverter':                                    
                        if value[32] != 0:
                            line = get_avg(start_time,no_col,value,name_head)
                            lines.append(line)
                        else:
                            print('Skipping ',str(start_time),' as Total Energy is 0')
                    else:
                        line = get_avg(start_time,no_col,value,name_head)
                        lines.append(line)            
                
                write_file.writelines(lines)
                print('Writing '+ write_path)
             

def meter(name_head,folder_name,file_field):
    tz = pytz.timezone('Asia/Kolkata')
    while True:
        today_date = datetime.datetime.now(tz).date()
        curr_date = determine_date()
        write_path = determine_path(curr_date,folder_name,file_field)
        read_path = write_path.replace('Dropbox/FlexiMC_Data/Gen1_Data', 'Data/Flexi_Raw_Data')
        
        try:
            if not os.path.exists(read_path):
                raise Exception('Empty')
            
                
        except Exception as e:
            if curr_date > today_date:
                print('Last file - Exiting while loop')
                break
            else:
                print('No file present - '+ str(e))
                continue

        read_write_ops(name_head,read_path,write_path,curr_date)      
        
            

set_date()
meter('inverter','Inverter_1','I1')
set_date()
meter('inverter','Inverter_2','I2')
set_date()
meter('inverter','Inverter_3','I3')
set_date()
meter('inverter','Inverter_4','I4')
set_date()
meter('inverter','Inverter_5','I5')

set_date()
meter('MFM','ESS_2','ESS_2')
set_date()
meter('MFM','ESS_3','ESS_3')
set_date()
meter('MFM','ESS_10','ESS_10')
set_date()
meter('MFM','AUX_1','AUX_1')
set_date()
meter('MFM','AUX_2','AUX_2')
set_date()
meter('MFM','AUX_3','AUX_3')

set_date()
meter('WMS','WMS','WMS')

print('DONE!')


        

    
